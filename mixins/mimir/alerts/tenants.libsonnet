local utils = import 'mixin-utils/utils.libsonnet';

(import 'alerts-utils.libsonnet') {
  local alertGroups = [
    {
      name: 'mimir_tenant_alerts',
      rules: [
        {
          alert: $.alertName('TenantNotSendingSamples'),
          'for': '5m',
          expr: |||
            # This will notify on tenants/users that we have not received samples from in the last 60 seconds.
            time() - max by (user) (cortex_distributor_latest_seen_sample_timestamp_seconds > 0) > 60
          |||,
          labels: {
            severity: 'critical',
          },
          annotations: {
            message: 'We have not received samples from teant {{ $labels.user }} in the last 5minutes' % $._config,
          },
        },
        {
          alert: $.alertName('TenantIngestionRateLimit'),
          'for': '15m',
          expr: |||
            # This will notify on tenants/users that are approaching their ingestion_rate limit.
            sum by (user) (rate(cortex_distributor_received_samples_total[1m]))
            /
            (
                max by (user) (cortex_limits_overrides{limit_name="ingestion_rate"})
                or
                max(cortex_limits_defaults{limit_name="ingestion_rate"})
             ) * 100 > 80
          |||,
          labels: {
            severity: 'critical',
          },
          annotations: {
            message: 'Teant {{ $labels.user }} has used {{ $value | humanizePercentage }} of its ingestion_rate limit.' % $._config,
          },
        },
        {
          alert: $.alertName('TenantMaxRuleGroupLimit'),
          'for': '15m',
          expr: |||
            # This will notify on tenants/users that are approaching their ruler_max_rule_groups_per_tenant limit.
            count by (user) (sum by (user, rule_group) (cortex_prometheus_rule_group_rules))
            /
            (
                max by (user) (cortex_limits_overrides{limit_name="ruler_max_rule_groups_per_tenant"})
                or
                max(cortex_limits_defaults{limit_name="ruler_max_rule_groups_per_tenant"})
            ) * 100 > 80
          |||,
          labels: {
            severity: 'critical',
          },
          annotations: {
            message: 'Teant {{ $labels.user }} has used {{ $value | humanizePercentage }} of its ruler_max_rule_groups_per_tenant limit.' % $._config,
          },
        },
        {
          alert: $.alertName('TenantGlobalSeriesLimit'),
          'for': '15m',
          expr: |||
            # This will notify on tenants/users that are approaching the maximum number of in-memory series across the cluster before replication.
            sum by (user) (
              (
                cortex_ingester_memory_series_created_total
                - cortex_ingester_memory_series_removed_total
              )
              / on(cluster, namespace) group_left
              max by (cluster, namespace) (cortex_distributor_replication_factor)
            )
            /
            (
                max by (user) (cortex_limits_overrides{limit_name="max_global_series_per_user"})
                or
                max(cortex_limits_defaults{limit_name="max_global_series_per_user"})
            ) * 100
          |||,
          labels: {
            severity: 'critical',
          },
          annotations: {
            message: 'Teant {{ $labels.user }} has used {{ $value | humanizePercentage }} its max_global_series_per_user limit.' % $._config,
          },
        },
      ],
    }
  ],

  local merged = $.mergedLabels($._config.additionalAlertLabels, alertGroups),
  groups+: $.withRunbookURL('https://grafana.com/docs/mimir/latest/operators-guide/mimir-runbooks/#%s', merged),
 }
