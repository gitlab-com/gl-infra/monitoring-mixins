{
  _config+:: {
    // jobSelector is added to all queries to idenfity metrics from
    // the prometheus servers.
    jobSelector: 'job="prometheus"',

    // Set to true if using prometheus in agent mode.
    prometheusAgentMode: true,

    // additional selectors are passed through to all queries to provide
    // further selector customization.
    // Supply as normal promql comma seperated label selectors
    // Example: additionalSelectors = 'foo="bar",fruit="oranges"'
    additionalSelectors: '',

    // Additional labels to add to aggregatoins (sum, etc)
    additionalAggregationLabels: '',

    // prometheusHAGroupLabels is a string with comma-separated labels
    // that are common labels of instances belonging to the same
    // high-availability group of Prometheus servers, i.e. identically
    // configured Prometheus servers. Include not only enough labels
    // to identify the members of the HA group, but also all common
    // labels you want to keep for resulting HA-group-level alerts.
    //
    // If this is set to an empty string, no HA-related alerts are applied.
    prometheusHAGroupLabels: '',

    // nameLabel is used in alerts to reference the instance name.
    // this can be updated if you prefer another label like fqdn or host.
    prometheusNameLabel: '{{$labels.instance}}',

    // If you run Prometheus on Kubernetes with the Prometheus
    // Operator, you can make use of the configured target labels for
    // nicer naming:
    // prometheusName: '{{$labels.namespace}}/{{$labels.pod}}'

    // prometheusHAGroupName is inserted into annotations to name an
    // HA group. All labels used here must also be present in
    // prometheusHAGroupLabels above.
    prometheusHAGroupName: '{{$labels.job}}',

    // nonNotifyingAlertmanagerRegEx can be used to mark Alertmanager
    // instances that are not part of the Alertmanager cluster
    // delivering production notifications. This is important for the
    // PrometheusErrorSendingAlertsToAnyAlertmanager alert. Otherwise,
    // a still working test or auditing instance could mask a full
    // failure of all the production instances. The provided regular
    // expression is matched against the `alertmanager` label.
    // Example: @'http://test-alertmanager\..*'
    nonNotifyingAlertmanagerRegEx: @'',


    // Set runbook key for alerts
    alertRunbookURLPrefix: 'https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/prometheus/troubleshooting.md#%s',
    alertRunbookKey: 'runbook_url',

    // Override default alert severities
    alertSeverityMapping: {
      critical: 'critical',
      warning: 'warninig',
    },

    // additional labels to append to all alerts.
    alertAdditionalLabels: {},
    // additional annotations to append to all alerts.
    alertAdditionalAnnotations: {},


    // allows overriding metadata per alert
    // Example:
    // alertMetadataOverrides: {
    //   PrometheusBadConfig: {
    //     labels: {
    //       foo: 'bar',
    //     },
    //     annotations: {
    //       blue: 'green',
    //     },
    //   },
    // }
    alertMetadataOverrides: {},

    grafanaPrometheus: {
      prefix: 'Prometheus / ',
      tags: ['prometheus-mixin'],
      // The default refresh time for all dashboards, default to 60s
      refresh: '60s',
    },

    additionalRules: [],
    additionalAlerts: [],

    selectors: if self.additionalSelectors == '' then self.jobSelector else std.format('%(jobSelector)s, %(additionalSelectors)s', self),
  },
}
