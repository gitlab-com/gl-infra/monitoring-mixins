{

  // withRunbookURL - Add/Override the runbook_url annotations for all alerts inside a list of rule groups.
  // - url_format: an URL format for the runbook, the alert name will be substituted in the URL.
  // - groups: the list of rule groups containing alerts.
  // - annotation_key: the key to use for the annotation whose value will be the formatted runbook URL.
  withRunbookURL(url_format, groups, annotation_key='runbook_url')::
    local update_rule(rule) =
      if std.objectHas(rule, 'alert')
      then rule {
        annotations+: {
          [annotation_key]: url_format % rule.alert,
        },
      }
      else rule;
    [
      group {
        rules: [
          update_rule(alert)
          for alert in group.rules
        ],
      }
      for group in groups
    ],

  removeRuleGroup(ruleName):: {
    local removeRuleGroup(rule) = if rule.name == ruleName then null else rule,
    local currentRuleGroups = super.groups,
    groups: std.prune(std.map(removeRuleGroup, currentRuleGroups)),
  },

  removeAlertRuleGroup(ruleName):: {
    prometheusAlerts+:: $.removeRuleGroup(ruleName),
  },

  removeRecordingRuleGroup(ruleName):: {
    prometheusRules+:: $.removeRuleGroup(ruleName),
  },

  overrideAlerts(overrides):: {
    local overrideRule(rule) =
      if 'alert' in rule && std.objectHas(overrides, rule.alert)
      then rule + overrides[rule.alert]
      else rule,
    local overrideInGroup(group) = group { rules: std.map(overrideRule, super.rules) },
    prometheusAlerts+:: {
      groups: std.map(overrideInGroup, super.groups),
    },
  },

  removeAlerts(alerts):: {
    local removeRule(rule) =
      if 'alert' in rule && std.objectHas(alerts, rule.alert)
      then {}
      else rule,
    local removeInGroup(group) = group { rules: std.map(removeRule, super.rules) },
    prometheusAlerts+:: {
      groups: std.prune(std.map(removeInGroup, super.groups)),
    },
  },

  mergedLabels(labels, groups):: [
    obj {
      rules: [
        rule { labels+: labels }
        for rule in obj.rules
      ],
    }
    for obj in groups
  ],

  mergedAnnotations(annotations, groups):: [
    obj {
      rules: [
        rule { annotations+: annotations }
        for rule in obj.rules
      ],
    }
    for obj in groups
  ],

}
