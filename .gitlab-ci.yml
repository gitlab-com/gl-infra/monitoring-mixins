# Mixins
.matrix:
  - TYPE: mixins
    DIR:
      - mimir
      - prometheus
  - TYPE: jsonnet-libs
    DIR: 
      - mixin-utils

stages:
  - docker
  - validate
  - release

default:
  image: 
    name: ${CI_REGISTRY}/gitlab-com/gl-infra/monitoring-mixins/mixin-tools:1.0.1
    entrypoint: [""]

.id_tokens:
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://vault.gitlab.net

validate:
  stage: validate
  parallel:
    matrix: !reference [.matrix]
  variables:
    PACKAGE_PATH: ${TYPE}/${DIR}
  script: |
    cd ${PACKAGE_PATH}
    echo "Validating ${PACKAGE_PATH}"
  rules:
    - changes:
        - ${TYPE}/${DIR}/**/*
      if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"

release:
  stage: release
  image: node:lts
  extends:
    - .id_tokens
  parallel:
    matrix: !reference [.matrix]
  secrets:
    GITLAB_TOKEN:
      file: false
      vault: access_tokens/gitlab-com/${CI_PROJECT_PATH}/semantic-release/token@ci
  variables:
    PACKAGE_PATH: ${TYPE}/${DIR}
  before_script:
    - npm install
  script:
    - cp .releaserc.json ${PACKAGE_PATH}/
    - pwd
    - ls
    - cd ${PACKAGE_PATH}
    - npx semantic-release
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - ${TYPE}/${DIR}/**/*

.docker:
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_DIR: tools

docker::scan:
  stage: docker
  image: docker:26.0.2
  extends: [.docker]
  services:
    - docker:26.0.2-dind
  variables:
    DOCKERFILE: "Dockerfile"
    IMAGE: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
    TRIVY_NO_PROGRESS: "true"
    TRIVY_CACHE_DIR: ".trivycache/"
  before_script:
    - cd ${DOCKER_DIR}
    - export TRIVY_VERSION=$(wget -qO - "https://api.github.com/repos/aquasecurity/trivy/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/')
    - echo $TRIVY_VERSION
    - wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -
  allow_failure: true
  script:
    # Build image
    - docker build -t $IMAGE .
    # Build reports
    - ./trivy config --exit-code 0 --format template --template "@contrib/gitlab-codequality.tpl" -o ../gl-code-quality-report.json $DOCKERFILE
    - ./trivy image --exit-code 0 --format template --template "@contrib/gitlab.tpl" -o ../gl-container-scanning-report.json $IMAGE
    # Print reports
    - ./trivy config --exit-code 0 $DOCKERFILE
    - ./trivy image --exit-code 0 $IMAGE
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
      changes:
        - ${DOCKER_DIR}/Dockerfile
  cache:
    paths:
      - .trivycache/
  artifacts:
    reports:
      container_scanning: gl-container-scanning-report.json
      codequality: gl-code-quality-report.json
    expire_in: 1 week


docker::release:
  stage: docker
  image: docker:26.0.2
  extends:
    - .id_tokens
    - .docker
  services:
    - docker:26.0.2-dind
  secrets:
    GITLAB_TOKEN:
      file: false
      vault: access_tokens/gitlab-com/${CI_PROJECT_PATH}/semantic-release/token@ci
  variables:
    DOCKER_REGISTRY_USER: ${CI_REGISTRY_USER}
    DOCKER_REGISTRY_PASSWORD: ${CI_REGISTRY_PASSWORD}
  before_script:
    - apk add --update npm git
    - npm install
  script:
    # - cp .releaserc.json ${DOCKER_DIR}/
    - cd ${DOCKER_DIR}
    - npx semantic-release
  rules:
    - if: $CI_COMMIT_REF_PROTECTED == "true" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE != "schedule"
      changes:
        - ${DOCKER_DIR}/Dockerfile
