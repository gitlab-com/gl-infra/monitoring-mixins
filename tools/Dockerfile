# Tool versions
ARG JSONNET_VERSION=0.20.0
ARG GO_VERSION=1.22.2-alpine
ARG ALPINE_VERSION=3.19.1

# Build mixtool
FROM golang:${GO_VERSION} AS builder

# Mixtool currently has no release logic so we install from the repo
ARG MIXTOOL_VERSION=main
ARG JB_VERSION=0.5.1

RUN go install -a github.com/monitoring-mixins/mixtool/cmd/mixtool@${MIXTOOL_VERSION} && \
    go install -a github.com/jsonnet-bundler/jsonnet-bundler/cmd/jb@v${JB_VERSION}

# Build jsonnet
FROM bitnami/jsonnet:${JSONNET_VERSION} AS jsonnet
FROM alpine:${ALPINE_VERSION}

COPY --from=jsonnet /opt/bitnami/jsonnet/bin/jsonnet /usr/local/bin
COPY --from=jsonnet /opt/bitnami/jsonnet/bin/jsonnetfmt /usr/local/bin
COPY --from=builder /go/bin/mixtool /usr/local/bin
COPY --from=builder /go/bin/jb /usr/local/bin

RUN apk add --no-cache bash findutils yq make

ENTRYPOINT ["/bin/ash"]
